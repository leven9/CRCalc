#include "inc/crc.h"

Crc::Crc()
{
}

Crc::~Crc()
{
}

const string &Crc::name() const
{
    return m_name;
}

const string &Crc::pascal() const
{
    return m_pascal;
}

uint64_t Crc::poly() const
{
    return m_poly;
}

uint64_t Crc::init() const
{
    return m_init;
}

uint64_t Crc::xorout() const
{
    return m_xorout;
}

bool Crc::refin() const
{
    return m_refin;
}

bool Crc::refout() const
{
    return m_refout;
}
