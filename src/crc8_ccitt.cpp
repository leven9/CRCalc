#include "inc/crc8_ccitt.h"

CRC8CCITT::CRC8CCITT()
{
    m_name = "CRC8/CCITT";
    m_pascal = "x8 + x2 + x + 1";
    m_poly = 0x07;
    m_init = 0x00;
    m_xorout = 0x00;
    m_refin = false;
    m_refout = false;
}

uint64_t CRC8CCITT::crcCalc(const uint8_t *data, int64_t len)
{
    if (NULL == data) {
        return -1;
    }
    if (len <= 0) {
        return -1;
    }

    uint8_t crc = 0x00;
    const uint8_t *p = data;

    static const uint8_t crc8_ccitt_small_table[16] = {
        0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15,
        0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d
    };

    for (int64_t i = 0; i < len; i++) {
        crc ^= p[i];
        crc = (crc << 4) ^ crc8_ccitt_small_table[crc >> 4];
        crc = (crc << 4) ^ crc8_ccitt_small_table[crc >> 4];
    }

    return crc;
}
