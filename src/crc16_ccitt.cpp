#include "inc/crc16_ccitt.h"

CRC16CCITT::CRC16CCITT()
{
    m_name = "CRC16/CCITT";
    m_pascal = "x16 + x12 + x5 + 1";
    m_poly = 0x1021;
    m_init = 0x0000;
    m_xorout = 0x0000;
    m_refin = true;
    m_refout = true;
}

uint64_t CRC16CCITT::crcCalc(const uint8_t *data, int64_t len)
{
    if (NULL == data) {
        return -1;
    }
    if (len <= 0) {
        return -1;
    }

    uint16_t crc = m_init;

    for (; len > 0; len--) {
        uint8_t e, f;

        e = crc ^ *data++;
        f = e ^ (e << 4);
        crc = (crc >> 8) ^ (f << 8) ^ (f << 3) ^ (f >> 4);
    }

    return crc;
}
