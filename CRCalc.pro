QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

RC_ICONS = favicon.ico

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src\crc8_ccitt.cpp \
    src\crc16_modbus.cpp \
    src\crc16_ccitt.cpp \
    src\crc32_ieee.cpp \
    src\crc.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    inc\crc8_ccitt.h \
    inc\crc16_modbus.h \
    inc\crc16_ccitt.h \
    inc\crc32_ieee.h \
    inc\crc.h \
    mainwindow.h \

FORMS += \
    mainwindow.ui

TRANSLATIONS += \
    CRCalc_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
