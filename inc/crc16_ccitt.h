#ifndef CRC16_CCITT_H
#define CRC16_CCITT_H

#include "crc.h"

class CRC16CCITT : public Crc
{
public:
    CRC16CCITT();

    // Crc interface
public:
    virtual uint64_t crcCalc(const uint8_t *data, int64_t len);
};

#endif // CRC16_CCITT_H
