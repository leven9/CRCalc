#ifndef CRC16_MODBUS_H
#define CRC16_MODBUS_H

#include "crc.h"

class CRC16Modbus : public Crc
{
public:
    CRC16Modbus();

    // Crc interface
public:
    virtual uint64_t crcCalc(const uint8_t *data, int64_t len);
};

#endif // CRC16_MODBUS_H
