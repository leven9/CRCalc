#ifndef CRC8CCITT_H
#define CRC8CCITT_H

#include "crc.h"

class CRC8CCITT : public Crc
{
public:
    CRC8CCITT();

    // Crc interface
public:
    virtual uint64_t crcCalc(const uint8_t *data, int64_t len);
};

#endif // CRC8CCITT_H
