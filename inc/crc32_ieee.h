#ifndef CRC32_H
#define CRC32_H

#include "crc.h"

class CRC32IEEE : public Crc
{
public:
    CRC32IEEE();

    // Crc interface
public:
    virtual uint64_t crcCalc(const uint8_t *data, int64_t len);
};

#endif // CRC32_H
