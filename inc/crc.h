#ifndef CRC_H
#define CRC_H

#include <string>
using namespace std;

class Crc
{
public:
    Crc();
    virtual ~Crc();

    const string &name() const;
    const string &pascal() const;
    uint64_t poly() const;
    uint64_t init() const;
    uint64_t xorout() const;
    bool refin() const;
    bool refout() const;

    virtual uint64_t crcCalc(const uint8_t *data, int64_t len) = 0;

protected:
    string m_name;
    string m_pascal;
    uint64_t m_poly;
    uint64_t m_init;
    uint64_t m_xorout;
    bool m_refin;
    bool m_refout;

private:

};

#endif // CRC_H
