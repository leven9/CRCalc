#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class Crc;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    qint64 m_fileMaxSize;

    QList<Crc *> m_crcList;
    Crc *m_currentCrc;

private:
    void initCrcList();
    void initWindow();

private slots:
    void on_nameComboBox_currentIndexChanged(int index);

    void on_cleanPushButton_clicked();
    void on_copyPushButton_clicked();
    void on_selectPushButton_clicked();
    void on_stopPushButton_clicked();
};

#endif // MAINWINDOW_H
