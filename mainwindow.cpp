#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QFileDialog>

#include "inc/crc8_ccitt.h"
#include "inc/crc16_ccitt.h"
#include "inc/crc16_modbus.h"
#include "inc/crc32_ieee.h"

#include <QDebug>
#define MAINWINDOW_DEBUG

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_fileMaxSize(32 * 1024 * 1024),
    m_currentCrc(nullptr)
{
    ui->setupUi(this);

    setWindowTitle("CRC 计算器");
    initCrcList();
    initWindow();
}

MainWindow::~MainWindow()
{
    delete ui;

    qDeleteAll(m_crcList);
    m_crcList.clear();
}

void MainWindow::initCrcList()
{
    m_crcList.append(new CRC8CCITT());
    m_crcList.append(new CRC16CCITT());
    m_crcList.append(new CRC16Modbus());
    m_crcList.append(new CRC32IEEE());
}

void MainWindow::initWindow()
{
    ui->nameComboBox->setEnabled(true);
    ui->pascalLineEdit->setEnabled(false);
    ui->polyLineEdit->setEnabled(false);
    ui->initLineEdit->setEnabled(false);
    ui->xoroutLineEdit->setEnabled(false);
    ui->refinCheckBox->setEnabled(false);
    ui->refoutCheckBox->setEnabled(false);

    for (auto index = 0; index < m_crcList.count(); index++) {
        ui->nameComboBox->addItem(QString::fromStdString(m_crcList.at(index)->name()));
    }

    on_nameComboBox_currentIndexChanged(0);
}

void MainWindow::on_nameComboBox_currentIndexChanged(int index)
{
    ui->nameComboBox->setCurrentIndex(index);
    ui->pascalLineEdit->setText(
                QString::fromStdString(m_crcList.at(index)->pascal()));
    ui->polyLineEdit->setText(
                "0x" + QString::number(m_crcList.at(index)->poly(), 16).toUpper());
    ui->initLineEdit->setText(
                "0x" + QString::number(m_crcList.at(index)->init(), 16).toUpper());
    ui->xoroutLineEdit->setText(
                "0x" + QString::number(m_crcList.at(index)->xorout(), 16).toUpper());
    ui->refinCheckBox->setChecked(
                m_crcList.at(index)->refin());
    ui->refoutCheckBox->setChecked(
                m_crcList.at(index)->refout());

    m_currentCrc = m_crcList.at(index);
}

void MainWindow::on_cleanPushButton_clicked()
{
    ui->infoTextEdit->clear();
}

void MainWindow::on_copyPushButton_clicked()
{
}

void MainWindow::on_selectPushButton_clicked()
{
    QString fileFull = QFileDialog::getOpenFileName(this, "Open file", "/");
    if (fileFull.isEmpty()) {
        return;
    }

    QFileInfo fileInfo = QFileInfo(fileFull);
    QFile file(fileFull);

    QString fileName = fileInfo.fileName();
    qint64 fileSize = fileInfo.size();
    quint64 fileCrc = 0x00;
    QByteArray byteArray;

    if (fileSize > m_fileMaxSize) {
        return;
    }
    if (!file.open(QIODevice::ReadOnly)) {
        return;
    }
    byteArray = file.readAll();
    if (byteArray.size() != fileSize) {
        return;
    }
    if (nullptr == m_currentCrc) {
        return;
    }
    fileCrc = m_currentCrc->crcCalc((const uint8_t *)byteArray.data(), fileSize);

    ui->infoTextEdit->clear();
    ui->infoTextEdit->append("Nmae: " + fileName);
    ui->infoTextEdit->append("Size: " + QString::number(fileSize));
    ui->infoTextEdit->append("CRC:  " + QString::number(fileCrc, 16).toUpper());
}

void MainWindow::on_stopPushButton_clicked()
{
}
